<?php

abstract class Heading
{
    protected string $body;
    protected int $level;

    public function __construct(string $body, int $level = null)
    {
        $this->body = $body;
        $this->level = $level ?? 1;
    }
}