<?php

class HeadingMarkdown extends Heading
{
    public function getOutput(): string
    {
        return str_repeat("#", $this->level)
            . $this->body
            . PHP_EOL;
    }
}