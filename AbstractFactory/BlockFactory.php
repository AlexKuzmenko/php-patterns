<?php

interface BlockFactory
{
    public function makeHeading(array $data): Heading;

    public function makeParagraph(array $data): Paragraph;
}