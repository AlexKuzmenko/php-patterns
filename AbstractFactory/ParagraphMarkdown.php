<?php

class ParagraphMarkdown extends Paragraph
{
    public function getOutput(): string
    {
        return PHP_EOL
            . str_replace("\n", "", $this->body)
            . PHP_EOL
            . PHP_EOL;
    }
}