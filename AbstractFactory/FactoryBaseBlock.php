<?php

abstract class FactoryBaseBlock
{
    protected function validateHeading(array $data): void
    {
        if (!isset($data['body'])) {
            throw new Exception("Heading must have a body");
        }
    }

    protected function validateParagraph(array $data): void
    {
        if (!isset($data['body'])) {
            throw new Exception("Paragraph must have a body");
        }
    }
}