<?php

class FactoryMarkdownBlock extends FactoryBaseBlock implements BlockFactory
{

    public function makeHeading(array $data): Heading
    {
        $this->validateHeading($data);
        return new HeadingMarkdown($data['body'], $data['level']);
    }

    public function makeParagraph(array $data): Paragraph
    {
        $this->validateHeading($data);
        return new ParagraphMarkdown($data['body']);
    }
}