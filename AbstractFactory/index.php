<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

spl_autoload_register(function ($class_name) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class_name).'.php';
    include $file;
});

$data = include "data.php";
class Page
{
    public static function outputContent(array $content, string $format): string
    {
        $output = null;
        switch ($format) {
            case 'html': $factory = new FactoryHTMLBlock(); break;
            case 'markdown': $factory = new FactoryMarkdownBlock(); break;
        }

        foreach ($content as $item) {
            $block = null;
            switch ($item['type']) {
                case 'heading':
                    $block = $factory->makeHeading($item['data']);
                    break;
                case 'paragraph':
                    $block = $factory->makeParagraph($item['data']);
                    break;
            }

            if ($block !== null) {
                $output .= $block->getOutput();
            }
        }

        return $output;
    }
}

echo Page::outputContent($data, "markdown");