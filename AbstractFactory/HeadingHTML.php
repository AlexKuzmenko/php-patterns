<?php

class HeadingHTML extends Heading
{
    public function getOutput(): string
    {
        return "<h" . $this->level . ">"
            . $this->body
            . "</h" . $this->level . ">";
    }
}