<?php
return [
    [
        "type" => "heading",
        "data" => [
            "level" => 1,
            "body" => "The primary heading",
        ]
    ],
    [
        "type" => "heading",
        "data" => [
            "level" => 2,
            "body" => "The secondary heading",
        ]
    ],
    [
        "type" => "paragraph",
        "data" => [
            "body" => "I am a paragraph",
        ]
    ],
    [
        "type" => "paragraph",
        "data" => [
            "body" => "I am also a paragraph",
        ]
    ],
];