<?php

abstract class Paragraph
{
    protected string $body;

    public function __construct(string $body)
    {
        $this->body = $body;
    }

}