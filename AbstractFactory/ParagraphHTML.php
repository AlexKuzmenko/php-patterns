<?php

class ParagraphHTML extends Paragraph
{
    public function getOutput(): string
    {
        return "<p>" . $this->body . "</p>";
    }
}