<?php

interface Block
{
    public function getOutput(): string;
}