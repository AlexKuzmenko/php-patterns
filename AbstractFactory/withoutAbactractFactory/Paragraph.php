<?php

class Paragraph
{
    protected string $body;

    public function __construct(string $body)
    {
        $this->body = $body;
    }

    public function getOutput(): string
    {
        return "<p>" . $this->body . "</p>";
    }
}