<?php
return [
    [
        "type" => "heading",
        "data" => [
            "level" => 1,
            "body" => "The primary heading",
        ]
    ],
    [
        "type" => "paragraph",
        "data" => [
            "body" => "I am a paragraph",
        ]
    ],
    [
        "type" => "paragraph",
        "data" => [
            "body" => "I am also a paragraph",
        ]
    ],
];