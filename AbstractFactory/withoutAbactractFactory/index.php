<?php
//ini_set('display_errors', '1');
//ini_set('display_startup_errors', '1');
//error_reporting(E_ALL);

spl_autoload_register(function ($class_name) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class_name).'.php';
    include $file;
});

$data = include "data.php";
class Page
{
    public static function outputContent(array $content, string $format): string
    {
        $output = null;

        foreach ($content as $item) {
            $block = null;
            switch ($item['type']) {
                case 'heading':
                    if (!isset($item['data']['body'])) {
                        throw new Exception("Heading must have a body");
                    }
                    $block = new Heading($item['data']['body'], $item['data']['level']);
                    break;
                case 'paragraph':
                    if (!isset($item['data']['body'])) {
                        throw new Exception("Paragraph must have a body");
                    }
                    $block = new Paragraph($item['data']['body']);
                    break;
            }

            if ($block !== null) {
                $output .= $block->getOutput();
            }
        }

        return $output;
    }
}

echo Page::outputContent($data, "html");