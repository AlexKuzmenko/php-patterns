<?php

class FactoryHTMLBlock extends FactoryBaseBlock implements BlockFactory
{

    public function makeHeading(array $data): Heading
    {
        $this->validateHeading($data);
        return new HeadingHTML($data['body'], $data['level']);
    }

    public function makeParagraph(array $data): Paragraph
    {
        $this->validateHeading($data);
        return new ParagraphHTML($data['body']);
    }
}