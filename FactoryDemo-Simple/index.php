<?php
spl_autoload_register(function($className) {
    if (is_file($className . ".php")) {
        include $className . ".php";
    }
});

$car1 = CarFactory::getCar();
$car1->move();

$car2 = CarFactory::getCar();
$car2->move();
