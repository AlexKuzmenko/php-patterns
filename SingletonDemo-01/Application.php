<?php


class Application
{
    private static $instance;
    private $userName;

    private function __construct()
    {

    }

    public static function getInstance(): Application
    {
        if (empty(self::$instance)) {
            self::$instance = new Application();
        }
        return self::$instance;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function setUserName($name)
    {
        $this->userName = $name;
    }
}
