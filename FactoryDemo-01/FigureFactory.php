<?php


abstract class FigureFactory
{
    abstract public function createFigure(): Figure;

    public function square():void
    {
        $figure = $this->createFigure();
        $square = $figure->getSquare();
        echo $square . "<br>";
    }
}