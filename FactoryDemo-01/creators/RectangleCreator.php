<?php

namespace creators;

use \Figure;
use \FigureFactory;
use \figures\Rectangle;

class RectangleCreator extends FigureFactory
{
    private float $sideA, $sideB;

    public function __construct(float $sideA, float $sideB)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
    }

    public function createFigure(): Figure
    {
        return new Rectangle($this->sideA, $this->sideB);
    }
}