<?php

namespace creators;

use \Figure;
use \FigureFactory;
use \figures\Triangle;

class TriangleCreator extends FigureFactory
{
    private float $sideA, $sideB, $sideC;

    public function __construct(float $sideA, float $sideB, float $sideC)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
        $this->sideC = $sideC;
    }

    public function createFigure(): Figure
    {
        return new Triangle($this->sideA, $this->sideB, $this->sideC);
    }
}