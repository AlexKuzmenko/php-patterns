<?php

abstract class Figure
{

    abstract function getSquare(): float;

}