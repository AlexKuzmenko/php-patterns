<?php

namespace figures;

use \Figure;

class Rectangle extends Figure
{
    public float $sideA, $sideB;

    public function __construct($sideA, $sideB)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
    }

    public function getSquare(): float
    {
        return $this->sideA * $this->sideB;
    }
}