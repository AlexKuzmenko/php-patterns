<?php

namespace figures;

use \Figure;

class Triangle extends Figure
{
    public float $sideA, $sideB, $sideC;

    public function __construct($sideA, $sideB, $sideC)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
        $this->sideC = $sideC;
    }

    public function getSquare(): float
    {
        $p = ($this->sideA + $this->sideB + $this->sideC) / 2;

        return sqrt($p * ($p - $this->sideA) * ($p - $this->sideB) * ($p - $this->sideC));
    }
}