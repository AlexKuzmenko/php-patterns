<?php

spl_autoload_register(function ($class_name) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class_name).'.php';
    include $file;
});

use creators\RectangleCreator;
use creators\TriangleCreator;


function clientCode(FigureFactory $creator)
{
    $creator->square();
}

clientCode(new RectangleCreator(5, 6));
clientCode(new RectangleCreator(3, 2));
clientCode(new TriangleCreator(2, 3, 4));
clientCode(new TriangleCreator(3, 4, 5));