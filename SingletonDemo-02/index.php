<?php
require("Preferences.php");

use patterns\Singleton\Preferences;

$preferences = Preferences::getInstance();
$preferences->setProperty("name", "John");

unset($preferences); //Видалення посилання

$preferences2 = Preferences::getInstance();
echo $preferences2->getProperty("name") . "\n"; //
